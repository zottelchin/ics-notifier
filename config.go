package main

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"strconv"
	"time"
)

var config = struct {
	calendar struct {
		URL      string
		Username string
		Password string
	}
	notify struct {
		offset   time.Duration
		interval time.Duration
	}
	updateInterval int
}{}

func loadConfig() {
	log.Info().Msg("Loading Config")

	// Load Config for ICS Access
	config.calendar.URL = os.Getenv("CAL_URL")
	config.calendar.Username = os.Getenv("CAL_USER")
	config.calendar.Password = os.Getenv("CAL_PASS")
	if config.calendar.URL == "" {
		log.Error().Msg("Calendar URL not specified")
		os.Exit(4)
	}

	// Set Offset in hours based on Env, defaults to 12h
	offsetstr := os.Getenv("EVENT_OFFSET")
	if offsetstr == "" {
		config.notify.offset = time.Hour * 12
	} else {
		offset, _ := strconv.Atoi(offsetstr)
		// Test if Env was Int
		if offsetstr != strconv.Itoa(offset) {
			log.Error().Str("Env", offsetstr).Int("Processed", offset).Msg("Invalid offset specified! Should be numeric value")
			os.Exit(4)
		}
		config.notify.offset = time.Hour * time.Duration(offset)
	}

	// Set Interval in Minutes based on Env, defaults to 15m
	intervalstr := os.Getenv("EVENT_INTERVAL")
	if intervalstr == "" {
		config.notify.interval = 15 * time.Minute
	} else {
		interval, _ := strconv.Atoi(intervalstr)
		// Test if Env was Int
		if intervalstr != strconv.Itoa(interval) {
			log.Error().Str("Env", intervalstr).Int("Processed", interval).Msg("Invalid interval specified! Should be numeric value")
			os.Exit(4)
		}
		config.notify.interval = time.Duration(interval) * time.Minute
	}

	// Set update interval
	updatestr := os.Getenv("UPDATE_INTERVAL")
	if updatestr == "" {
		config.updateInterval = int(config.notify.interval.Minutes() /2)
		if config.updateInterval < 1 {
			config.updateInterval = 1
		}
		if config.updateInterval > 30 {
			config.updateInterval = 30
		}
	} else {
		update, _ := strconv.Atoi(updatestr)
		// Test if Env was Int
		if updatestr != strconv.Itoa(update) {
			log.Error().Str("Env", updatestr).Int("Processed", update).Msg("Invalid update interval specified! Should be numeric value")
			os.Exit(4)
		}
		config.updateInterval = update
	}

	log.Info().Msg("Config loaded")
	log.Debug().Str("Calendar URL", config.calendar.URL).Send()
	log.Debug().Str("Calendar User", config.calendar.Username).Send()
	log.Debug().Str("Calendar Password", config.calendar.Password).Send()
	log.Debug().Str("Event Lookup Offset", fmt.Sprintf("%.0f Hours", config.notify.offset.Hours())).Send()
	log.Debug().Str("Event Lookup Interval", fmt.Sprintf("%.0f Minutes", config.notify.interval.Minutes())).Send()
	log.Debug().Str("Update Interval", fmt.Sprintf("%d Minutes", config.updateInterval)).Send()
}
