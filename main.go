package main

import (
	"codeberg.org/zottelchin/ics-notifier/notifier"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"strconv"
	"time"
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "02.01.06 15:04"})
	if _, ok := os.LookupEnv("DEBUG"); ok {
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
		log.Debug().Msg("Debugging enabled")
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
	zonename, _ := time.Now().Zone()
	log.Info().Str("Zeitzone", zonename).Msg("The Timezone is set to this value")

	// Initialize everything - load Config from Environment-Variables
	loadConfig()
	for _, n := range notifier.AllNotifiers {
		log.Debug().Str("Notifier", n.Name()).Msg("Setting up notifier")
		n.Setup()
	}

	// Check for upcoming Events based every half interval
	log.Info().Str("Check Interval", strconv.Itoa(config.updateInterval) + "m" ).Msgf("Starting checkloop")
	for {
		go checkUpcoming()
		log.Debug().Str("Next Check", time.Now().Add(time.Duration(config.updateInterval)).Format("02.01.06 15:04")).Send()
		time.Sleep(time.Duration(config.updateInterval) * time.Minute)
	}
}

func checkUpcoming() {
	// Load events
	events := getEvents()
	if len(events) == 0 {
		log.Debug().Msg("No Events in timeframe found")
		return
	}

	// Call each Notifier for every Event
	log.Debug().Msg("Processing events")
	for _, e := range events {
		log.Info().Str("Summary", e.Summary).Str("ID", e.Uid).Msg("Event in Timeframe found")
		for _, n := range notifier.AllNotifiers {
			n.Notify(e)
		}
	}
}
