package main

import (
	"github.com/apognu/gocal"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

func getEvents() []gocal.Event {
	log.Debug().Msg("Loading Events")

	// Get File from Server using basic auth
	client := &http.Client{}
	req, err := http.NewRequest("GET", config.calendar.URL, nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed create request")
		return []gocal.Event{}
	}
	if config.calendar.Username != "" {
		req.SetBasicAuth(config.calendar.Username, config.calendar.Password)
		log.Debug().Msg("Using basic auth")
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("Failed to request ics content")
		return []gocal.Event{}
	}
	if resp.StatusCode != 200 {
		// TODO: improve Error handling
		log.Error().Int("Code", resp.StatusCode).Msg("Unnormal response code")
		return []gocal.Event{}
	}

	// Specify Timeframe for Event-Search
	start := time.Now().Add(config.notify.offset)
	end := time.Now().Add(config.notify.offset).Add(config.notify.interval)
	log.Debug().Str("End", end.Format("02.01.06 15:04")).Str("Start", start.Format("02.01.06 15:04")).Msg("Searching for Events in Timeframe")

	//Parse ICS-File and set timeframe
	cal := gocal.NewParser(resp.Body)
	cal.Start, cal.End = &start, &end
	// ToDo: handle error
	cal.Parse()

	// return the Events in this timeframe
	return cal.Events
}
