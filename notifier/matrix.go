package notifier

import (
	"fmt"
	"github.com/apognu/gocal"
	"github.com/rs/zerolog/log"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
	"os"
)

type MatrixNotifier struct {
	Enabled bool
	Homeserver string
	Username string
	Password string
	Room id.RoomID
}

func (m *MatrixNotifier) Name() string {
	return "MatrixNotifier"
}

func (m *MatrixNotifier) Setup()  {
	m.Enabled = os.Getenv("MATRIX_ENABLED") == "1" || os.Getenv("MATRIX_ENABLED") == "true"
	if !m.Enabled {
		log.Info().Msg("MatrixNotifier is not enabled, skipping setup")
		return
	}
	log.Debug().Msg("MatrixNotifier is enabled, loading config from Env")

	m.Username = os.Getenv("MATRIX_USER")
	if m.Username == "" {
		log.Error().Msg("MatrixNotifier enabled with empty username")
		os.Exit(4)
	}

	m.Password = os.Getenv("MATRIX_PASS")
	if m.Password == "" {
		log.Error().Msg("MatrixNotifier enabled with empty password")
		os.Exit(4)
	}

	m.Homeserver = os.Getenv("MATRIX_HOMESERVER") //"zottel/dev"
	if m.Homeserver == "" {
		log.Error().Msg("MatrixNotifier enabled with empty homeserver")
		os.Exit(4)
	}

	m.Room = id.RoomID(os.Getenv("MATRIX_ROOM"))
	if m.Room == "" {
		log.Error().Msg("MatrixNotifier enabled with empty roomID")
		os.Exit(4)
	}

	log.Debug().Interface("MatrixNotifier", m).Send()
	log.Info().Msg("MatrixNotifier finished setup")
}

func (m *MatrixNotifier) Notify (event gocal.Event) {
	if !m.Enabled {
		log.Info().Msg("Skipping MatrixNotifier cause it's disabled")
		return
	}

	client, err := mautrix.NewClient(m.Homeserver, "", "")
	if err != nil {
		log.Error().Err(err).Msg("Failed to connect to homeserver")
		return
	}

	log.Error().Str("homeserver", client.HomeserverURL.String()).Send()

	_, err = client.Login(&mautrix.ReqLogin{

		Type: mautrix.AuthTypePassword,
		Identifier: mautrix.UserIdentifier{Type: mautrix.IdentifierTypeUser, User: m.Username},
		Password: m.Password,
		StoreCredentials: true,
	})
	if err != nil {
		log.Error().Err(err).Msg("Failed to login")
		return
	}

	content := fmt.Sprintf("Am %s findet \"%s\" um %s Uhr statt.", event.Start.Format("02.01"), event.Summary, event.Start.Format("15.04"))
	_, err = client.SendNotice(m.Room, content)
	if err != nil {
		log.Error().Err(err).Msg("Failed to send notification")
		return
	}

	client.Logout()
}