package notifier

import (
	"github.com/apognu/gocal"
	"github.com/peterbourgon/diskv"
	"github.com/rs/zerolog/log"
	"strings"
)

// Notifier Interface definition for a modulare Notifier structure
type Notifier interface {
	Name() string
	Setup()
	Notify(event gocal.Event)
}

// Array with Notifier reference
// Add your notifier here to register it
var AllNotifiers = []Notifier{
	&MailNotifier{},
	&TelegramNotifier{},
	&MatrixNotifier{},
	&ListmonkNotifier{},
}

var store = diskv.New(diskv.Options{BasePath: "storage"})

func success(key string) {
	err := store.Write(saveName(key), nil)
	if err != nil {
		log.Error().Err(err).Send()
	}
}

func previouslyNotified(key string) bool {
	return store.Has(saveName(key))
}

func saveName(input string) string {
	return strings.ReplaceAll(input, "/", "-")
}
