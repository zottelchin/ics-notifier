package notifier

import (
	"fmt"
	"github.com/apognu/gocal"
	"github.com/rs/zerolog/log"
	mail "github.com/xhit/go-simple-mail"
	"os"
	"strconv"
	"strings"
)

type MailNotifier struct {
	Enabled    bool
	Host       string
	Port       int
	Username   string
	Password   string
	EncryptTLS bool
	Sender     string
	Receivers  []string
}

func (m *MailNotifier) Name() string {
	return "MailNotifier"
}

func (m *MailNotifier) Setup() {
	m.Enabled = os.Getenv("MAIL_ENABLED") == "1" || os.Getenv("MAIL_ENABLED") == "true"
	if !m.Enabled {
		log.Info().Msg("MailNotifier is not enabled, skipping setup")
		return
	}
	log.Debug().Msg("MailNotifier is enabled, loading config from Env")

	// Setting SMTP HOST
	m.Host = os.Getenv("MAIL_HOST")
	if m.Host == "" {
		log.Error().Msg("MailNotifier enabled with empty host")
		os.Exit(4)
	}

	// Setting SMTP Port
	portstr := os.Getenv("MAIL_PORT")
	if portstr == "" {
		log.Debug().Msg("MailNotifier no port specified, defaults to 25")
		m.Port = 25
	} else {
		m.Port, _ = strconv.Atoi(portstr)
		// Test if Env was Int
		if portstr != strconv.Itoa(m.Port) {
			log.Error().Str("Env", portstr).Int("Processed", m.Port).Msg("Invalid port specified! Should be numeric value")
			os.Exit(4)
		}
	}

	// Setting Username and Password
	m.Username = os.Getenv("MAIL_USER") //"zottel/dev"
	if m.Username == "" {
		log.Error().Msg("MailNotifier enabled with empty username")
		os.Exit(4)
	}
	m.Password = os.Getenv("MAIL_PASS") // "DJ8sgBzv7X7lFPwMr7HPO34A"
	if m.Password == "" {
		log.Error().Msg("MailNotifier enabled with empty password")
		os.Exit(4)
	}

	// Setting Encryption
	m.EncryptTLS = os.Getenv("MAIL_TLS") == "1" || os.Getenv("MAIL_TLS") == "true"

	// Setting Sender and Recivers
	m.Sender = os.Getenv("MAIL_FROM")
	if m.Sender == "" {
		log.Error().Msg("MailNotifier enabled without from adress")
		os.Exit(4)
	}
	to := os.Getenv("MAIL_TO")
	if to == "" {
		log.Error().Msg("MailNotifier enabled without receivers")
		os.Exit(4)
	}
	m.Receivers = strings.Split(to, ",")

	log.Debug().Interface("MailNotifier", m).Send()
	log.Info().Msg("MailNotifier finished setup")
}

func (m *MailNotifier) Notify(event gocal.Event) {
	if !m.Enabled {
		log.Info().Msg("Skipping MailNotifier cause it's disabled")
		return
	}

	server := mail.NewSMTPClient()
	server.Host = m.Host
	server.Port = m.Port
	server.Username = m.Username
	server.Password = m.Password
	if m.EncryptTLS {
		server.Encryption = mail.EncryptionTLS
	}
	client, err := server.Connect()
	if err != nil {
		log.Error().Err(err).Msg("Failed to connect to SMTP-Server")
		return
	}

	// Send Mail to every Receiver separately for privacy-reasons
	for _, to := range m.Receivers {
		id := "mail-"+event.Summary+"-"+event.Start.String()+"-"+to
		if previouslyNotified(id) {
			log.Debug().Str("ID", id).Msgf("Skipping Mail to %s. Previously informed", to)
			continue
		}
		err = mail.NewMSG().
			SetFrom(m.Sender).
			AddTo(to).
			SetSubject("Erinnerung für "+event.Summary).
			SetBody(mail.TextPlain, fmt.Sprintf(tmpl, event.Start.Format("2.1.06"), event.Summary, event.Start.Format("15:04"))).
			Send(client)
		if err != nil {
			log.Error().Err(err).Str("to", to).Msg("Failed to send Mail to Address")
		} else {
			log.Debug().Str("to", to).Msg("Send Mail")
			success(id)
		}
	}

	client.Close()
	log.Info().Msg("All Mails send, if not previously notified.")
}

const tmpl = `Denk daran, dass am %s der Termin '%s' um %s Uhr stattfindet.`
