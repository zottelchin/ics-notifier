package notifier

import (
	"fmt"
	"github.com/apognu/gocal"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/rs/zerolog/log"
	"os"
	"strconv"
)

type TelegramNotifier struct {
	Enabled bool
	Token   string
	Chat    int64
}

func (t *TelegramNotifier) Name() string {
	return "TelegramNotifier"
}

func (t *TelegramNotifier) Setup() {
	t.Enabled = os.Getenv("TG_ENABLED") == "1" || os.Getenv("TG_ENABLED") == "true"
	if !t.Enabled {
		log.Info().Msg("Telegram is not enabled, skipping setup")
		return
	}
	log.Debug().Msg("Telegram is enabled, loading config from Env")

	t.Token = os.Getenv("TG_TOKEN")
	if t.Token == "" {
		log.Error().Msg("Telegram Token not specified")
		os.Exit(4)
	}
	t.Chat, _ = strconv.ParseInt(os.Getenv("TG_CHAT"), 10, 64)
	if t.Chat == 0 {
		log.Error().Int64("parsed", t.Chat).Str("Env", os.Getenv("TG_CHAT")).Msg("Telegram Chat ID is not set or parsed correct")
		os.Exit(4)
	}

	log.Debug().Interface("Telegram", t).Send()
	log.Info().Msg("TelegramNotifier finished setup")
}

func (t *TelegramNotifier) Notify(event gocal.Event) {
	if !t.Enabled {
		log.Info().Msg("Skipping TelegramNotifier cause it's disabled")
		return
	}
	id := "telegram-"+event.Summary+"-"+event.Start.String()
	if previouslyNotified(id) {
		log.Debug().Str("ID", id).Msg("Skipping Telegram. Previously informed")
		return
	}

	bot, err := tgbotapi.NewBotAPI(t.Token)
	if err != nil {
		log.Error().Err(err).Msg("Bot creation failed")
		return
	}

	content := fmt.Sprintf("Am *%s* findet \"*%s*\" um *%s Uhr* statt.", event.Start.Format("02.01"), event.Summary, event.Start.Format("15.04"))
	msg := tgbotapi.NewMessage(t.Chat, content)
	msg.ParseMode = tgbotapi.ModeMarkdown
	_, err = bot.Send(msg)
	if err != nil {
		log.Error().Err(err).Msg("Message sending failed")
		return
	}

	success(id)
	log.Info().Msg("Telegram Message was send to group")
}
