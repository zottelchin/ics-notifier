package notifier

import (
	"github.com/apognu/gocal"
	"github.com/go-resty/resty/v2"
	"github.com/rs/zerolog/log"
	"os"
	"strconv"
	"strings"
)

type ListmonkNotifier struct {
	Enabled  bool
	BaseURL  string
	Lists    []int
	Username string
	Password string
}

type CampResp struct {
	Data struct {
		ID     int
		Status string
	}
	Message string
}

func (n *ListmonkNotifier) Name() string {
	return "ListmonkNotifier"
}

func (n *ListmonkNotifier) Setup() {
	n.Enabled = os.Getenv("MONK_ENABLED") == "1" || os.Getenv("MONK_ENABLED") == "true"
	if !n.Enabled {
		log.Info().Msg("ListmonkNotifier is not enabled, skipping setup")
		return
	}
	log.Debug().Msg("ListmonkNotifier is enabled, loading config from Env")

	n.Username = os.Getenv("MONK_USER")
	if n.Username == "" {
		log.Error().Msg("ListmonkNotifier enabled with empty username")
		os.Exit(4)
	}

	n.BaseURL = os.Getenv("MONK_URL")
	if n.BaseURL == "" {
		log.Error().Msg("ListmonkNotifier enabled with empty url")
		os.Exit(4)
	}

	n.Password = os.Getenv("MONK_PASS")
	if n.Password == "" {
		log.Error().Msg("ListmonkNotifier enabled with empty password")
		os.Exit(4)
	}

	lists := os.Getenv("MONK_LISTS")
	if lists == "" {
		log.Error().Msg("ListmonkNotifier enabled with empty lists")
		os.Exit(4)
	} else {
		for _, i := range strings.Split(lists, ",") {
			iInt, err := strconv.Atoi(i)
			if err != nil {
				log.Error().Str("Env", lists).Str("item", i).AnErr("Err", err).Msg("Failed to convert string to int")
			}
			n.Lists = append(n.Lists, iInt)
		}
		if len(n.Lists) == 0 {
			log.Error().Str("env", lists).Msg("ListmonkNotifier has empty list after parsing")
			os.Exit(4)
		}
	}

	log.Debug().Interface("ListmonkNotifier", n).Send()
	log.Info().Msg("ListmonkNotifier finished setup")
}

func (n *ListmonkNotifier) Notify(event gocal.Event) {
	if !n.Enabled {
		log.Info().Msg("Skipping ListmonkNotifier cause it's disabled")
		return
	}

	id := "monk-" + event.Summary + "-" + event.Start.String()
	if previouslyNotified(id) {
		log.Debug().Str("ID", id).Msg("Skipping Listmonk. Previously informed")
		return
	}

	client := resty.New()
	client.SetBaseURL(n.BaseURL)
	client.SetBasicAuth(n.Username, n.Password)

	// Setup Campaign
	res := CampResp{}
	client.R().SetBody(map[string]interface{}{
		"name":         "Cal-Notifier",
		"subject":      "Erinnerung für " + event.Summary,
		"lists":        n.Lists,
		"content_type": "markdown",
		"body": "## Erinnerung für einen Kalendertermin!\n\nTermin: " + event.Summary + "\n\nDatum: " + event.Start.Format("2.1.20	06") + "\n\nUhrzeit: " + event.Start.Format("15:04") + "Uhr\n\nOrt: " + event.Location,
		"type": "regular",
	}).SetResult(&res).Post("/api/campaigns")
	log.Debug().Int("ID", res.Data.ID).Str("status", res.Data.Status).Send()

	// Execute Campaign
	_, err := client.R().SetFormData(map[string]string{
		"status": "running",
	}).SetResult(&res).Put("/api/campaigns/" + strconv.Itoa(res.Data.ID) + "/status")
	log.Debug().Int("ID", res.Data.ID).Str("status", res.Data.Status).Send()
	if err != nil {
		log.Error().Err(err).Str("Msg", res.Message).Msg("Failed to execute campaign")
		return
	}

	success(id)
	log.Info().Int("ID", res.Data.ID).Msg("Campain send!")
}
