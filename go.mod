module codeberg.org/zottelchin/ics-notifier

go 1.16

require (
	github.com/apognu/gocal v0.8.0
	github.com/channelmeter/iso8601duration v0.0.0-20150204201828-8da3af7a2a61 // indirect
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/google/btree v1.0.1 // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible
	github.com/rs/zerolog v1.21.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/xhit/go-simple-mail v2.2.2+incompatible
	maunium.net/go/mautrix v0.9.14
)
