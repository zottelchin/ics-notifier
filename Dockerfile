FROM golang:alpine AS build
RUN apk --no-cache add ca-certificates tzdata
COPY go.mod /build/
WORKDIR /build
RUN go mod download
COPY *.go /build/
COPY notifier/ /build/notifier/
RUN CGO_ENABLED=0 go build -ldflags '-s -w' -o /out/ics-notifier .

FROM scratch
WORKDIR /
COPY --from=build /out/ /
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENV TZ="Europe/Berlin"
ENTRYPOINT ["/ics-notifier"]