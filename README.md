### ⚠️ Use at own risk ⚠️

--------
[![Docker Pulls](https://img.shields.io/badge/Docker%20Image-download-blue?style=for-the-badge)](https://hub.docker.com/r/zottelchin/ics-notifier)

# ICS Notifier

Notify users via Email or Telegram (at the moment the only options) about upcoming events. Configuration is done via Environment variables to allow easy deployment via docker (and docker-compose).

Further notifiers can be added via pull-request. See notifier/telegram.go as example and add the notifier to the notifier/provider.go.

I tested the Notifier only with Nextcloud Calendar. Use the URL with `?export` at the end to get the ics file as a result. Should work with every link to an ics-file.


## Confiuration

| Environment     | Requiered | Description                                                                                                               |
|-----------------|-----------|---------------------------------------------------------------------------------------------------------------------------|
| CAL_URL         | yes       | The URL to load the ics-file from.                                                                                        |
| CAL_USER        | no        | Basic auth username to access the CAL_URL. Basic Auth is only enabled when username is specified.                         |
| CAL_PASS        | no        | Basic auth password to access the CAL_URL.                                                                                |
| EVENT_OFFSET    | no        | Numeric value to specify the number of hours, for offsetting the timeframe from the time of executation. Defaults to `12` |
| EVENT_INTERVAL  | no        | Numeric value to specify timeframe width in minutes to search in. Defaults to `15`                                        |
| UPDATE_INTERVAL | no        | Numeric value to specify the time between two lookups in minutes. Defaults to `EVENT_INTERVAL` / 2 in the interval [1,30] |
|                 |           |                                                                                                                           |
| MAIL_ENABLED    | no        | To enable mail notifications set to `1` or `true`. If not set the other vars are not required.                            |
| MAIL_HOST       | yes       |                                                                                                                           |
| MAIL_PORT       | no        | Defaults to 25                                                                                                            |
| MAIL_USER       | yes       |                                                                                                                           |
| MAIL_PASS       | yes       |                                                                                                                           |
| MAIL_TLS        | no        | To enable TLS encryption set to `1` or `true`.                                                                            |
| MAIL_FROM       | yes       | specify the sender adress                                                                                                 |
| MAIL_TO         | yes       | receivers for notifications, can be multiple: list with ',' seperating mails without spaces                               |
|                 |           |                                                                                                                           |
| TG_ENABLED      | no        | Set to `1` or `true` to enable telegram notification. If not set the other vars are not requiered.                        |
| TG_TOKEN        | yes       | Telegram Bot Token, can be acquired via @Botfather                                                                        |
| TG_CHAT         | yes       | Chat-ID of the targetchat. Help: https://stackoverflow.com/a/32572159                                                     |
|                 |           |                                                                                                                           |
| MONK_ENABLED    | yes       | To enable listmonk notifications set to `1` or `true`. If not set the other vars are not required.                        |
| MONK_USER       | yes       | Listmonk Username                                                                                                         |
| MONK_USER       | yes       | Listmonk Password                                                                                                         |
| MONK_URL        | yes       | BaseURL of your Listmonk instance                                                                                         |
| MONK_LISTS      | yes       | commasep ids for listmonk lists                                                                                           |

For persistent storage, map the `/storage` folder from the container to the local file system. This will preserve the notified users when recreating the container.